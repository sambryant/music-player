import numpy as np
import math
import progressbar
import sys
from abc import abstractmethod
from pyaudio import PyAudio, paUInt8
from numpy import sin, pi, exp, log
from collections import namedtuple
from typing import List, Tuple, Dict
from freqs import *
from song import *
from tone import *

def simple():
  s = Song()
  s.add_note(Note(0, 1, PureTone(C4)))
  s.add_note(Note(1, 1, PureTone(E4)))
  s.add_note(Note(2, 1, PureTone(G4)))
  s.add_note(Note(3, 1, PureTone(E4)))
  s.add_note(Note(4, 1, PureTone(C4)))
  s.play()

def one_summers_day():
  b = 1
  s = Song(22050)
  sm = 0 # start measure
  note_type = PureToneEnvelope

  # Measure 1
  s.add_note_seq2(note_type, sm, b, {
    0: [(2, E4), (2, C4), (2, A4), (4, F2)],
    1: [(2, F3)],
    2: [(2, A4)],
    3: [(2, D4)],
    4: [(2, F3)],
    5: [(2, A4)],
    6: [(2, G4)],
  }, beat_type=0.5)

  # Measure 2
  s.add_note_seq2(note_type, sm + 1, b, {
    0: [(2, E4), (2, B4), (2, G3), (4, G2)],
    1: [(2, D3)],
    2: [(2, G3)],
    3: [(2, D4)],
    4: [(2, D3)],
    5: [(2, G3)],
    6: [(2, B4)],
  }, beat_type=0.5)

  # Measure 3
  s.add_note_seq2(note_type, sm + 2, b, {
    0: [(2, D4), (2, G3), (2, C3)],
    1: [(2, E3)],
    2: [(2, G3)],
    3: [(2, C4)],
    4: [(2, E3)],
    5: [(2, G3)],
    6: [(2, G4)],
  }, beat_type=0.5)

  # Measure 4
  s.add_note_seq2(note_type, sm + 3, b, {
    0: [(2, D4), (2, G3), (2, A3)],
    1: [(2, E3)],
    2: [(2, G3)],
    3: [(2, C4)],
    4: [(2, E3)],
    5: [(2, G3)],
    6: [(2, B3)],
  }, beat_type=0.5)

  # Measure 5
  s.add_note_seq2(note_type, sm + 4, b, {
    0: [(2, A4), (2, F3), (2, F2)],
    1: [(2, E4), (2, C4), (2, A4)],
    2: [(2, E4), (2, C4)],
    3: [(2, E4), (2, C4)],
    4: [(2, E4), (2, C4), (2, A4), (2, F2)],
    5: [(2, D4), (2, A4)],
    6: [(2, E4), (2, C4), (2, A4), (2, F3)],
    7: [(2, A5), (2, D4), (2, B4)]
  }, beat_type=0.5)

  # Measure 6
  s.add_note_seq2(note_type, sm + 5, b, {
    0: [(2, E4), (2, B4), (2, G3), (2, G2)],
    1: [(1, D4), (1, G3), (1, D3)],
    1.5: [(2.5, D4), (2.5, G3)],
    4: [(2, G2)],
    5: [(2, B3)],
    6: [(2, D3)],
    7: [(2, G3)],
  }, beat_type=0.5)

  # Measure 7
  s.add_note_seq2(note_type, sm + 6, b, {
    0: [(2, C3),],
    1: [(2, G3), (2, D4), (2, E3)],
    2: [(2, G3), (2, D4)],
    3: [(2, G3), (2, D4)],
    4: [(2, G3), (2, D4), (2, C3)],
    5: [(2, G3), (2, C4)],
    6: [(2, G3), (2, D4), (2, E3)],
    7: [(2, G4), (2, C4), (2, G3)],
  }, beat_type=0.5)

  # Measure 8
  s.add_note_seq2(note_type, sm + 7, b, {
    0: [(2, D4), (2, G3), (2, E3), (2, A3)],
    1: [(2, C4), (2, G3)],
    2: [(2, B4), (2, G3)],
    3: [(4, A4), (4, E3)],
    5: [(2, E3)],
    6: [(2, C4)],
    7: [(2, B4)],
  }, beat_type=0.5)

  # Measure 9
  s.add_note_seq2(note_type, sm + 8, b, {
    0: [(2, C4), (2, A4), (2, D3)],
    1: [(2, C4), (2, A4)],
    2: [(2, C4), (2, A4)],
    3: [(2, C4), (2, A4)],
    4: [(2, C4), (2, A4), (2, F2)],
    5: [(3, C4), (2, A4)],
    6: [(2, F3)],
    7: [(1, A4)],
    7.5: [(1, B4)]
  }, beat_type=0.5)
  s.play()

if __name__ == '__main__':
  # simple()
  one_summers_day()

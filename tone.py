import numpy as np
import math
import progressbar
import sys
from abc import abstractmethod
from pyaudio import PyAudio, paUInt8
from numpy import sin, pi, exp, log
from collections import namedtuple
from typing import List, Tuple, Dict

class Tone():

  @abstractmethod
  def write(self, raw_samples, length, sample_rate):
    pass

class PureTone(Tone):

  def __init__(self, freq):
    self.freq = freq
    self.period = 1 / freq

  def write_chunk(self, chunk_values, chunk_samp_offset, note_samp_st, note_samp_ed, sample_rate):
    cs = len(chunk_values)

    rel_sam_st = max(note_samp_st - chunk_samp_offset, 0)
    rel_sam_ed = min(note_samp_ed - chunk_samp_offset, cs)
    def func(sample_index):
      offset_time = (sample_index + chunk_samp_offset - note_samp_st) / sample_rate
      return np.sin(2 * pi * self.freq * offset_time)
    samples = np.array(range(rel_sam_st, rel_sam_ed), dtype=int)
    chunk_values[samples] += func(samples)

  def write(self, raw_samples, time, length, sample_rate):
    num_periods = int(np.ceil(length / self.period))
    num_samples = int(num_periods * self.period * sample_rate)
    i_st = int(time * sample_rate)
    i_ed = int(i_st + num_samples)
    func = np.array([np.sin(2 * pi * self.freq * i / sample_rate) for i in range(num_samples)])
    raw_samples[i_st:i_ed] += func[:]

class PureToneEnvelope(Tone):

  def __init__(self, freq, k_factor=2):
    self.freq = freq
    self.period = 1 / freq
    self.k_factor = k_factor

  def write_chunk(self, chunk_values, chunk_samp_offset, note_samp_st, note_samp_ed, sample_rate):
    cs = len(chunk_values)

    rel_sam_st = max(note_samp_st - chunk_samp_offset, 0)
    rel_sam_ed = min(note_samp_ed - chunk_samp_offset, cs)
    def func(sample_index):
      t = (sample_index + chunk_samp_offset - note_samp_st) / sample_rate
      return exp( - self.k_factor * t) * np.sin(2 * pi * self.freq * t)
    samples = np.array(range(rel_sam_st, rel_sam_ed), dtype=int)
    chunk_values[samples] += func(samples)

  def write(self, raw_samples, time, length, sample_rate):
    num_periods = int(np.ceil(length / self.period))
    num_samples = int(num_periods * self.period * sample_rate)
    i_st = int(time * sample_rate)
    i_ed = int(i_st + num_samples)

    k = 1 / length
    def get(t):
      return exp(- 2 * k * t) * sin(2 * pi * self.freq * t)
    func = np.array([get(i / sample_rate) for i in range(num_samples)])
    raw_samples[i_st:i_ed] += func[:]


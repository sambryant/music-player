import numpy as np
import math
import progressbar
import sys
from abc import abstractmethod
from pyaudio import PyAudio, paUInt8
from numpy import sin, pi, exp, log
from collections import namedtuple
from typing import List, Tuple, Dict
from tone import *

CHUNK_SIZE = 8 * 1024
SAMPLE_RATE = 22050

class Note():

  def __init__(self, time, length, tone):
    self.time = time
    self.length = length
    self.tone = tone

  def get_end(self):
    return self.time + self.length

  def write_chunk_old(self, chunk_values, chunk_time_st, sample_rate):
    note_time_ed = self.time + self.length
    self.tone.write_chunk(chunk_values, chunk_time_st, self.time, note_time_ed, sample_rate)

  def write_chunk(self, chunk_values, chunk_sample_offset, sample_rate):
    note_time_ed = self.time + self.length
    self.tone.write_chunk(chunk_values, chunk_sample_offset, self.samp_st, self.samp_ed, sample_rate)

  def write(self, raw_samples, sample_rate):
    self.tone.write(raw_samples, self.time, self.length, sample_rate)

class Song():
  sample_rate: int
  chunk_size: int
  notes: List[Note]

  chunked_notes: List[List[Note]]

  def __init__(self, sample_rate=SAMPLE_RATE, chunk_size=CHUNK_SIZE):
    self.chunk_size = chunk_size
    self.sample_rate = sample_rate
    self.notes = []
    self.chunked_notes = []

  def _compute_note_chunks(self, note):
    samp_st = int(np.ceil(note.time * self.sample_rate))
    print('Warning: Compute end of sample by period')
    num_samples = int(np.ceil(self.sample_rate * note.length))
    samp_ed = samp_st + num_samples

    note.samp_st = samp_st
    note.samp_ed = samp_ed

    chunk_st = int(samp_st / self.chunk_size)
    chunk_ed = int(np.ceil(samp_ed / self.chunk_size))

    return list(range(chunk_st, chunk_ed))

  def add_note(self, note: Note):
    chunks = self._compute_note_chunks(note)

    # Extend number of chunks to fit note
    while len(self.chunked_notes) <= chunks[-1]:
      self.chunked_notes.append([])
    for chunk in chunks:
      self.chunked_notes[chunk].append(note)
    self.notes.append(note)

  def add_note_seq(self, cls, measure, beat_length, notes: List[Tuple[float, float, float]]):
    time = measure * beat_length * 4

    for (beat, length, freq) in notes:
      self.add_note(Note(time + beat*beat_length, length * beat_length, cls(freq)))

  def add_note_seq2(self, cls, measure, beat_length, notes: Dict[float, List[Tuple[float, float]]], beat_type=1.0):
    t0 = measure * beat_length * 4

    for time, note_list in notes.items():
      t = t0 + beat_type * time * beat_length
      for length, freq in note_list:
        self.add_note(Note(t, beat_type * length * beat_length, cls(freq)))

  def play(self):
    pa = PyAudio()
    stream = pa.open(
      format=paUInt8,
      channels=1,  # mono
      rate=self.sample_rate,
      output=True)

    # Figure out maximum number of simultaneous notes and use to normalize
    max_simul = max([len(notes) for notes in self.chunked_notes])

    print('Streaming chunks')
    for chunk, chunk_notes in enumerate(self.chunked_notes):
      raw_samples = np.zeros(self.chunk_size)
      for note in chunk_notes:
        note.write_chunk(raw_samples, self.chunk_size * chunk, self.sample_rate)
      raw_samples /= max_simul
      samples = (int(value * 0x7F + 0x80) for value in raw_samples)
      stream.write(bytes(samples))

    stream.stop_stream()
    stream.close()
    pa.terminate()
